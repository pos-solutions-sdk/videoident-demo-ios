# VideoIdent SDK iOS

VideoIdentSDK is an iOS framework that allows you to start the VideoIdent Service from any host app.

## Installation

Install the `VideoIdentSDK` using the CocoaPods dependency manager.

### CocoaPods

Use the CocoaPod `VideoIdentSDK`. Add the pod to you `Podfile`

```text
pod 'VideoIdentSDK'
```

and install it with

```text
pod install
```

## Integration

In `AppDelegate` import `VideoIdentSDK`

```swift
import VideoIdentSDK
```

### Configuration

The main entry point is the Singleton class `VideoIdentApp`.
`VideoIdentApp` provides configuration using the server URL and returning the VideoIdentSDK Start controller as an entry UIViewController.

Run the configuration by providing the server URL string and before initializing a Start controller.

```swift
// configure with URL string
VideoIdentApp.shared.configure(with: "https://URL_STRING")
```

You can do that inside the AppDelegate method:

```swift
func application(_:, didFinishLaunchingWithOptions:) -> Bool
```

When there is no `Target` provided a Default one will be used.

After configuring the Url a color scheme, localized strings and image assets could be added. This is an optional step.

```swift
// set configuration
VideoIdentApp.shared.set(configuration: "CONFIGURATION_NAME")
```

VideoIdentSDK supports customizable configuration for better flexibility and branding of the app.

Each `Configuration` consists of 3 files that can be used for configuration:

- CONFIGURATION_NAME.json
- CONFIGURATION_NAME.strings
- CONFIGURATION_NAME.xcassets file for images

#### CONFIGURATION_NAME.json

main Configuration definition file where you can define a brand logo and colors through the app:

```json
// json definition
{
    // name of the Configuration
    "name": "CONFIGURATION_NAME",

    // name of the Logo image from .xcassets file
    "logo": "LOGO_IMAGE_NAME",

    // colors to match the branding
    "primaryColor": "#5086FF",
    "primaryColorDark": "#4b7ef1",
    // allows skipping the user date scrren if data is already present
    "skipUserDataEntryData" : false
}
```

#### CONFIGURATION_NAME.strings

localization strings that override SDK Localization:

```swift
// strings definition
"start-view-controller.button-continue.title" = "";
"start-view-controller.text-view-help-text.text" = "";

"terms-view-controller.button-start.title" = "";

"loading-view-start.details-label.text" = "";

"end-view-controller.accepted-title-label.text" = "";

"end-view-controller.accepted-description-label.text" = "";

"end-view-controller.rejected-title-label.text" = "";

"end-view-controller.rejected-description-label.text" = "";
```

#### CONFIGURATION_NAME.cxassets

Assets file with images defined in the main definition file.

Then you can present the VideoIdentSDK view controller in the way that suits you the best.

### Starting the Service

Get the VideoIdentSDK Identification controller and then present it in the way it suits you the best.

```swift
let startController = VideoIdentApp.shared.identificationController(with: "GUID")

// example 1: present it modally
present(controller, animated: true)
```

```swift
// example 2: push it to the navigation stack
navigationController?.pushViewController(navigationController, animated: true)
```


## Notifications

`VideoIdentSDK` uses *NotificationCenter* to emit Events that you can subscribe to, to be aware of what the SDK's current status is.

```swift
.videoIdentVerificationCompleted
.videoIdentVerificationCanceled
.videoIdentVerificationError
```

To listen for the Events add the Observer to those Notifications:

```swift
NotificationCenter.default.addObserver(self, selector: #selector(videoIdentVerificationCompleted(_:.)), name: .videoIdentVerificationCompleted, object: nil)
```

Make sure to clean after and remove observers from NotificationCenter:

```swift
NotificationCenter.default.removeObserver(self)
```

It's common to register and remove observers in either:

- `viewDidLoad()` and `deinit`

- `viewWillAppear(_:)` and `viewWillDisappear(_:)`

## API Documentation

VideoIdentSDK publicly available API.

### Class `VideoIdentApp`

#### Method `configure(with:)`

```swift
public func configure(with url: String)
```

**Parameters:**

| Name | Type | Description  |
| - | - | - |
| url | String | The server url the Sdk connects to. This will be providet to you by POS |

Urls usually must end with "/" and the path usually end with "/POSIdent/".

#### Method `set(configuration:)`

```swift
public func set(configuration: String)
```

**Parameters:**

| Name | Type | Description  |
| - | - | - |
| configuration | String | Name of the target |

All config files (json, strings and xcassets) must have the same name (name of the target).

#### Method `identificationController()`

```swift
public func identificationController(with identificationGuid: String) -> UIViewController
```

**Parameters:**

| Name | Type | Description  |
| - | - | - |
| identificationGuid | String | Guid of the prepared identification |

Use this method to get an Identification controller responsible for starting the identification process. You can then present it as a modal controller or push it to the navigation stack, or present it in any other way you might need.

#### Method `cancelIdentification()`

```swift
public func cancelIdentification()
```

Use this method to cancel the running identification process:

#### Method `setLog(level:)`

```swift
public func setLog(level: Int)
```

**Parameters:**

| Name | Type | Description  |
| - | - | - |
| level | Int | Desired logging level |

Sets the current log level for the SDK. Default: 0 (Disabled).

### Extension `Notification.Name`

This is a list of all available Notifications that can be observed:

```swift
extension Notification.Name {
    static let videoIdentVerificationCompleted = Notification.Name("videoIdentVerificationCompleted")
    static let videoIdentVerificationCanceled = Notification.Name("videoIdentVerificationCanceled")
    static let videoIdentVerificationError = Notification.Name("videoIdentVerificationError")
}
```

## Example

Example integration of the `VideoIdentSDK`:

```swift
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        VideoIdentApp.shared.configure(with: "https://server/POSIdent/")
        VideoIdentApp.shared.set(configuration: "CONFIGURATION")
        VideoIdentApp.shared.setLog(level: 1)
        
        return true
    }
}

class ViewController: UIViewController {
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(videoIdentVerificationCompleted(_:)), name: .videoIdentVerificationCompleted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(videoIdentVerificationCanceled), name: .videoIdentVerificationCanceled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(videoIdentVerificationError(_:)), name: .videoIdentVerificationError, object: nil)
        
        startVideoIdentService(with: "GUID")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func startVideoIdentService(with identificationGuid: String)) {
        let controller = VideoIdentApp.shared.identificationController(with: identificationGuid)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func videoIdentVerificationCompleted(_ notification: Notification) {
        guard let userInfo = notification.userInfo as NSDictionary? else { return }
        
        print(userInfo["identification_status"] ?? "")
    }
    
    @objc func videoIdentVerificationError(_ notification: Notification) {
        guard let userInfo = notification.userInfo as NSDictionary? else { return }
        
        print(userInfo["identification_error"] ?? "")
    }

    @objc func videoIdentVerificationCanceled() {
        print("verification canceled")
    }
}
```