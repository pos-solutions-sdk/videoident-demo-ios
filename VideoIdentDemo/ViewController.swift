//
//  ViewController.swift
//  VideoIdentDemo
//
//  Created by Valentin Rep on 21.01.2021..
//

import UIKit
import Toast_Swift
import VideoIdentSDK

class ViewController: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var txtGUID: UITextField!
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        addObservers()
        hideKeyboardWhenTappedAround()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    private func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(videoIdentVerificationCompleted(_:)),
            name: .videoIdentVerificationCompleted, object: nil
        )
                
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(videoIdentVerificationCanceled),
            name: .videoIdentVerificationCanceled, object: nil
        )
                
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(videoIdentVerificationError(_:)),
            name: .videoIdentVerificationError, object: nil
        )
    }
    
    @IBAction func actionStartVideoIdent(_ sender: UIButton) {
        guard let identificationGuid = txtGUID.text else { return }
        
        VideoIdentApp.shared.set(configuration: "demo")
        startVideoIdentService(with: identificationGuid)
    }
    
    @IBAction func actionStartVideoIdentAlternate(_ sender: UIButton) {
        guard let identificationGuid = txtGUID.text else { return }
        
        VideoIdentApp.shared.set(configuration: "alternate")
        startVideoIdentService(with: identificationGuid)
    }
    
    // MARK: - VideoIdent SDK
    private func startVideoIdentService(with identificationGuid: String) {
        let controller = VideoIdentApp.shared.identificationController(with: identificationGuid)

        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController?.pushViewController(controller, animated: true)
         self.view.makeToast("This is a piece of toast")
    }
        
    @objc func videoIdentVerificationCompleted(_ notification: Notification) {
        navigationController?.popViewController(animated: true)
        
        if let userInfo = notification.userInfo as? [String: String] {
            print(userInfo["identification_status"] ?? "")
            let message = userInfo["identification_status"] ?? ""
              self.view.makeToast("SUCCES : \(message)")
        }
        
        if let customerStatus = notification.object as? CustomerStatus {
            print(customerStatus)
        }
    }
        
    @objc func videoIdentVerificationError(_ notification: Notification) {
        navigationController?.popViewController(animated: true)
        
        if let userInfo = notification.userInfo as? [String: String] {
            print(userInfo["identification_error"] ?? "")
            let message = userInfo["identification_error"] ?? ""

              self.view.makeToast("ERROR : \(message)")
        }
        
        // if let customerStatus = notification.object as? CustomerStatus {
        //     print(customerStatus)
        // }
      
    }

    @objc func videoIdentVerificationCanceled() {
        navigationController?.popViewController(animated: true)
        
        print("Verification canceled")
    }

    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard2))
        tap.cancelsTouchesInView = false            
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard2() {
        view.endEditing(true)
    }
}
